# syntax=docker/dockerfile:1

# Build image with :
# docker build --tag=x86-64-test-cifx-dev:stable .

# Create container :
# docker create --privileged --network=host --restart=no --env SSHPORT=2224 \
# --name x86-64-test-cifx-dev \
# --volume /sys:/sys \
# --volume /home/admin/Test_cifXDrv/opt/cifx:/opt/cifx \
# x86-64-test-cifx-dev:stable

# Start container :
# docker start x86-64-test-cifx-dev

# Use latest debian stable as base image
FROM debian:stable

# Labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.1" \
      description="Debian(stable) / cifX Drv Test Development"

# Version
ENV TEST_CIFX_DEV_VERSION 0.0.1

# Execute all commands as root
USER root

# Environment variables
ENV USER=test
ENV PASSWD="cifX"

# Install ssh, gcc, create specified user and make him sudo
RUN apt-get update \
    && apt-get install -y openssh-server build-essential sudo \
    && mkdir /var/run/sshd \
    && useradd --create-home --shell /bin/bash $USER \
    && echo $USER:$PASSWD | chpasswd \
    && adduser $USER sudo

# Install nano to edit files if necessary
RUN apt-get update \
    && apt-get install -y nano

# Install necessary stuff to compile cifX Driver
RUN apt-get update \
    && apt-get install -y \
          git cmake pkg-config \
          libpciaccess0 libpciaccess-dev

# Kernel space driver uio_netx has to be built on the host system
# Install cifX Device Driver Linux library and build some demo applications
RUN mkdir --parents /home/$USER/Hilscher/GitHub \
    && cd /home/$USER/Hilscher/GitHub \
    && git clone https://github.com/HilscherAutomation/nxdrvlinux.git nxdrvlinux \
    && cd nxdrvlinux \
    && cd libcifx && mkdir drv_build && cd drv_build \
    && cmake ../ \
    && make \
    && make install \
    && ldconfig \
    && cd ../../examples && mkdir demo_build && cd demo_build \
    && cmake ../ \
    && make

# Install necessary stuff for cifX Driver Python binding
RUN apt-get update \
    && apt-get install -y \
          python3 python3-venv python3-dev

RUN mkdir --parent /home/$USER/Python/cifX \
    && cd /home/$USER/Python/cifX \
    && git clone https://gitlab.com/ada-for-automation/python/cifx/cifx-binding.git . \
    && python3 -m venv ./venv \
    && . venv/bin/activate \
    && python3 -m pip install invoke \
    && python3 -m pip install cffi \
    && invoke build-cffi-cifx-linux \
    && invoke build-cffi-cifx

# Finally give ownership and rights
RUN chown -R $USER:$USER "/home/$USER"

# SSH port
EXPOSE 22

# Do entrypoint
COPY "./init.d/test-cifx/*" /etc/init.d/
RUN chmod +x "/etc/init.d/entrypoint.sh"
ENTRYPOINT ["/etc/init.d/entrypoint.sh"]

# Set STOPSIGNAL
STOPSIGNAL SIGTERM


