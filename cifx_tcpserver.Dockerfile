# syntax=docker/dockerfile:1

# Build image with :
# docker build --file=cifx_tcpserver.Dockerfile --tag=cifx-tcpserver:stable .

# Create container :
# docker create --privileged --network=host --restart=no \
# --name cifx-tcpserver \
# --volume /sys:/sys \
# --volume /home/admin/Test_cifXDrv/opt/cifx:/opt/cifx \
# cifx-tcpserver:stable

# Start container :
# docker start cifx-tcpserver

# Use latest debian stable as base image
FROM debian:stable

# Labeling
LABEL maintainer="slos@hilscher.com" \
      version="V0.0.1" \
      description="Debian(stable) / cifX TCP Server"

# Version
ENV CIFX_TCP_SERVER_VERSION 0.0.1

# Execute all commands as root
USER root

# Install cifX Driver
COPY --from=x86-64-test-cifx-dev:stable  \
    /usr/local/lib/libcifx.so.3.0.0 /usr/local/lib/

# cifX TCP Server path
ENV LOCATION="/home/test/Hilscher/GitHub/nxdrvlinux/examples/demo_build"

# Install cifX TCP Server
COPY --from=x86-64-test-cifx-dev:stable  \
    $LOCATION/cifx_tcpserver /root

# Install necessary stuff for cifX Driver
RUN apt-get update \
    && apt-get install -y \
          libpciaccess0 \
    && ldconfig

# cifX TCP Server port
EXPOSE 50111

# Do entrypoint
COPY "./init.d/cifx-tcpserver/*" /etc/init.d/
RUN chmod +x "/etc/init.d/cifx-tcpserver" \
    && chmod +x "/etc/init.d/entrypoint.sh"
ENTRYPOINT ["/etc/init.d/entrypoint.sh"]

# Set STOPSIGNAL
STOPSIGNAL SIGTERM

