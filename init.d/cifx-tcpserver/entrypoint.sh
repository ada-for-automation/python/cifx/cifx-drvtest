#!/bin/bash +e

# catch signals as PID 1 in a container

# SIGNAL-handler
term_handler() {
 
  echo "entrypoint : Terminating cifX TCP Server..."
  /etc/init.d/cifx-tcpserver stop
  echo "entrypoint : cifX TCP Server stopped..."

  exit 143; # 128 + 15 -- SIGTERM
}

# on callback, stop all started processes in term_handler
trap 'kill ${!}; term_handler' SIGINT SIGKILL SIGTERM SIGQUIT SIGTSTP SIGSTOP SIGHUP

# resolve HOST just in case
if ! ( grep -q "127.0.0.1 localhost localhost.localdomain ${HOSTNAME}" /etc/hosts > /dev/null);
then
  echo "127.0.0.1 localhost localhost.localdomain ${HOSTNAME}" >> /etc/hosts
fi

# run applications in the background
echo "entrypoint : Starting cifX TCP Server..."
/etc/init.d/cifx-tcpserver start &
echo "entrypoint : cifX TCP Server started..."

# wait forever not to exit the container
while true
do
  tail -f /dev/null & wait ${!}
done

exit 0
