#!/bin/bash +e

# catch signals as PID 1 in a container

# SIGNAL-handler
term_handler() {
 
  echo "Terminating ssh ..."
  /etc/init.d/ssh stop

  exit 143; # 128 + 15 -- SIGTERM
}

# on callback, stop all started processes in term_handler
trap 'kill ${!}; term_handler' SIGINT SIGKILL SIGTERM SIGQUIT SIGTSTP SIGSTOP SIGHUP

# resolve HOST just in case
if ! ( grep -q "127.0.0.1 localhost localhost.localdomain ${HOSTNAME}" /etc/hosts > /dev/null);
then
  echo "127.0.0.1 localhost localhost.localdomain ${HOSTNAME}" >> /etc/hosts
fi

# give user access to cifX
chmod o+rw /dev/uio0
chmod o+rw /sys/class/uio/uio0/device/config

# run applications in the background
echo "starting SSH server ..."
if [ "SSHPORT" ]; then
  #there is an alternative SSH port configured
  echo "the container binds the SSH server port to the configured port $SSHPORT"
  sed -i -e "s;#Port 22;Port $SSHPORT;" /etc/ssh/sshd_config
else
  echo "the container binds the SSH server port to the default port 22"
fi
/etc/init.d/ssh start &

# wait forever not to exit the container
while true
do
  tail -f /dev/null & wait ${!}
done

exit 0
